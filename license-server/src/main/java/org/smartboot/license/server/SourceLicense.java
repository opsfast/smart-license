/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: SourceLicense.java
 * Date: 2020-03-22
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.server;

import org.smartboot.license.client.common.LicenseConfig;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/21
 */
public class SourceLicense extends LicenseConfig {
    /**
     * 密文
     */
    private String encrypt;

    /**
     * 公钥
     */
    private String publicKey;

    /**
     * 私钥
     */
    private String privateKey;

    public SourceLicense(String applyDate, String expireDate, String original) {
        setApplyDate(applyDate);
        setExpireDate(expireDate);
        setOriginal(original);
    }

    public String getEncrypt() {
        return encrypt;
    }

    public void setEncrypt(String encrypt) {
        this.encrypt = encrypt;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}
